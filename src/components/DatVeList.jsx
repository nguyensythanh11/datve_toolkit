import React from 'react';
import DatVeChair from './DatVeChair';
import cn from 'classname';

const DatVeList = ({data}) => {
  return (
    <div className='DatVeList'>
        {
            data.map((hangGhe) => (
                <div key={hangGhe.hang} className='d-flex'>
                    <p className='text-center hangGhe' style={{width: "50px"}}>{hangGhe.hang}</p>
                    <div className = {cn('d-flex', {
                        "hangDau": hangGhe.hang == "",
                    })} style={{gap: "8px"}}>
                    {
                        hangGhe.danhSachGhe.map((ghe) => (
                            <DatVeChair data={ghe} key={ghe.soGhe}></DatVeChair>
                        ))
                    }
                    </div>
                </div>
            ))
        }
    </div>
  )
}

export default DatVeList