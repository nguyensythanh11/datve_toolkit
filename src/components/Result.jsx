import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { DatVeActions } from '../store/DatVe/slice';

const Result = () => {
    const { chairBookings } = useSelector((rootReducer) => rootReducer.DatVe);
    const dispatch =useDispatch();
  return (
    <div className='Result'>\
        <h2>DANH SÁCH GHẾ BẠN CHỌN</h2>
        <div className='chuThich d-flex'>
            <p className='hinhVuong hinhVuong1'></p>
            <p>Ghế đã đặt</p>
        </div>
        <div className='chuThich d-flex'>
            <p className='hinhVuong hinhVuong2'></p>
            <p>Ghế đang chọn</p>
        </div>
        <div className='chuThich d-flex'>
            <p className='hinhVuong hinhVuong3'></p>
            <p>Ghế chưa đặt</p>
        </div>
        <table  className='table table-bordered'>
            <thead>
                <tr>
                    <th>Số ghế</th>
                    <th>Giá</th>
                    <th>Hủy</th>
                </tr>
            </thead>
            <tbody>
                {
                    chairBookings.map((chair)=>(
                        <tr>
                            <td>{chair.soGhe}</td>
                            <td>{chair.gia}</td>
                            <td className='delete'>
                                <span onClick={() => { dispatch(DatVeActions.setChairBookings(chair)) }}>X</span>
                            </td>   
                        </tr>
                    ))
                }
                <tr>
                    <td className='tongTien'>Tổng tiền</td>
                    <td>{
                         chairBookings.reduce((total, chair) => total+=chair.gia, 0)    
                    }</td>
                </tr>
            </tbody>
        </table>
        <button className='btn btn-success' style={{fontWeight: 700}} onClick={() => { dispatch(DatVeActions.setPay()) }}>Thanh Toán</button>
    </div>
  )
}

export default Result