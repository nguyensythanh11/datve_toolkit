import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { DatVeActions } from '../store/DatVe/slice';
import cn from 'classname'

const DatVeChair = ({data}) => {
  const dispatch = useDispatch();
  const { chairBookings, chairBooked } = useSelector((rootReducer) => ( rootReducer.DatVe ));
  return (
    <div className='DatVeChair'>
        {
            <button style={{
                width: '50px', 
                marginBottom: '10px'
            }} className={cn('btn btn-outline-danger', {
              'booking': chairBookings.find((chair) => ( chair.soGhe == data.soGhe )),
              'booked': chairBooked.find((chair) => ( chair.soGhe == data.soGhe ))
            })}
              onClick={() => {
                dispatch(DatVeActions.setChairBookings(data))
              }}
            >{
              data.soGhe[0] >= 'A' && data.soGhe[0] <= 'J' ? data.soGhe.substring(1,3) : data.soGhe.substring(0,2)  
             }</button>
        }
    </div>
  )
}

export default DatVeChair