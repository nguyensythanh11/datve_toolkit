import {combineReducers} from 'redux'
import { DatVeReducer } from './DatVe/slice'

export const rootReducer = combineReducers({
    DatVe: DatVeReducer,
}) 