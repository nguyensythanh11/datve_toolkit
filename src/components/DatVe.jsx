import React from 'react'
import DatVeList from './DatVeList'
import Result from './Result'
import '../sass/style.scss'
import data from '../data/data.json'

const DatVe = () => {
  return (
    <div className="DatVe">
        <div className='overlay'></div>
        <div className="pt-4">
            <div className='row'>
                <div className="col-8 left">
                    <h1>ĐẶT VÉ XEM PHIM CYBERLEARN.VN</h1>
                    <div>
                        <p>Màn Hình</p>
                        <p className='manHinh'></p>
                        <p className='anhSang'></p>
                    </div>
                    <DatVeList data = {data}></DatVeList>
                </div>
                <div className="col-4 right">
                    <Result></Result>
                </div>
            </div>
        </div>
    </div>
  )
}

export default DatVe