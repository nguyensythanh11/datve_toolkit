import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    chairBookings: [],
    chairBooked: [],
}

const DatVeSlice = createSlice({
    name: 'DatVe',
    initialState,
    reducers: {
        setChairBookings: (state, action) => { 
            const index = state.chairBookings.findIndex((chair)=>(chair.soGhe == action.payload.soGhe));
            if(index == -1){
                state.chairBookings.push(action.payload);
            } else {
                state.chairBookings.splice(index, 1);
            }
        },
        setPay: (state, action) => { 
            state.chairBooked = [... state.chairBooked, ...state.chairBookings];
            state.chairBookings = [];
        },
    },
})

export const { actions: DatVeActions, reducer: DatVeReducer } = DatVeSlice